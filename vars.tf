variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "topic_arn" {
  description = "arn of the topic"
  type        = string
}

variable "subscriptions" {
  description = "add subscriptions to the topic"
  type        = map(object({
    protocol     = string
    endpoint_arn = string
  }))
}