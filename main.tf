resource "aws_sns_topic_subscription" "default" {
  for_each  = var.subscriptions
  topic_arn = var.topic_arn
  protocol  = each.value.protocol
  endpoint  = each.value.endpoint_arn
}